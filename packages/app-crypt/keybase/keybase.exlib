# Copyright 2016 Julien Durillon <julien.durillon@gmail.com>
# Copyright 2017 Thomas Anderson <tanderson@caltech.edu>
# Distributed under the terms of the GNU General Public License v2

require systemd-service [ systemd_files=[ ] systemd_user_files=[ packaging/linux/systemd ] ]

export_exlib_phases src_prepare src_compile src_install

SUMMARY="Keybase CLI tool"
HOMEPAGE="https://keybase.io/"

LICENCES="BSD-3"
SLOT="0"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.7.1]
    run:
        !app-crypt/kbfs [[
            description = [ kbfs got merged into keybase client ]
            resolution = uninstall-blocked-before
        ]]
"

keybase_src_prepare() {
    edo mkdir -p "${WORK}"/.gopath/src/github.com/keybase/
    edo ln -s "${WORK}" "${WORK}"/.gopath/src/github.com/keybase/client
    edo mkdir bin
}

keybase_build_one() {
    GO111MODULE="auto" edo go build \
        -o "${WORK}"/bin/${1} \
        -tags production \
        github.com/keybase/client/go/${2:-${1}}
}

keybase_src_compile() {
    export GOPATH="${WORK}"/.gopath

    keybase_build_one keybase
    keybase_build_one kbnm
    keybase_build_one kbfsfuse kbfs/kbfsfuse
    keybase_build_one kbfstool kbfs/kbfstool
    keybase_build_one keybase-redirector kbfs/redirector
    keybase_build_one git-remote-keybase kbfs/kbfsgit/git-remote-keybase
}

keybase_src_install() {
    dobin bin/*
    install_systemd_files
}


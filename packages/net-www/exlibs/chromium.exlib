# Copyright 2009, 2010 Elias Pipping <pipping@exherbo.org>
# Copyright 2009 Heiko Przybyl <zuxez@cs.tu-berlin.de>
# Copyright 2016-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# You'll find new releases here:
# - https://chromiumdash.appspot.com
# - https://chromereleases.googleblog.com

CHROMIUM_CHANNEL=${PN#chromium-}

MY_PN=chromium
MY_PNV=${MY_PN}-${PV}

if [ "${CHROMIUM_CHANNEL}" != "stable" ]; then
    MY_PN=${PN}
fi

require freedesktop-desktop gtk-icon-cache
require python [ blacklist=2 has_lib=false multibuild=false ]
require toolchain-funcs flag-o-matic ninja
require ffmpeg [ abis=[ 7 ] ]
require option-renames [ renames=[ 'va vaapi' ] ]

export_exlib_phases pkg_setup src_prepare src_configure src_compile src_test src_install pkg_postinst pkg_postrm

SUMMARY="The open-source project behind Google Chrome"
DESCRIPTION="Browser based on Blink and Google's V8 JavaScript engine."
HOMEPAGE="https://www.chromium.org/Home"
DOWNLOADS="
    https://storage.googleapis.com/chromium-browser-official/${MY_PNV}.tar.xz
    https://gsdview.appspot.com/chromium-browser-official/${MY_PNV}.tar.xz
"

LICENCES="
    BSD-3         [[ note = [ chromium itself ] ]]
    Apache-2.0    [[ note = [ skia ] ]]
    BSD-2         [[ note = [ bsdiff, bspatch ] ]]
    BSD-3         [[ note = [ dtoa, jscre, modp_b64, v8, webkit ] ]]
    GPL-2         [[ note = [ hunspell, 'Mozilla interface to Java Plugin APIs', npapi, nspr, nss ] ]]
    LGPL-2        [[ note = [ webkit ] ]]
    LGPL-2.1      [[ note = [ hunspell, 'Mozilla interface to Java Plugin APIs', npapi, nspr,
                              nss, webkit ] ]]
    MPL-1.1       [[ note = [ hunspell, 'Mozilla interface to Java Plugin APIs', npapi, nspr, nss ] ]]
    MIT           [[ note = [ harfbuzz ] ]]
    public-domain [[ note = [ lzma_sdk, sqlite ] ]]
"
SLOT="0"
MYOPTIONS="
    bindist
    cups
    pulseaudio
    suid-sandbox [[ description = [ Enable Chromium's SUID sandbox, only necessary for
        kernels built without user namespaces ] ]]
    vaapi [[ description = [ Enable support for GPU hardware acceleration using the Video Acceleration API ] ]]
    platform:
        amd64
        x86
    x86_cpu_features:
        sse2
    ( providers: jpeg-turbo )
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# the version of clang needed may not match the system default and we need to ensure the same version of
# llvm tools get used. So this locks the dependency to the specific version we know works and the
# buildtools smylinks are created from this version
LLVM_SLOT=19


# http://crbug.com/62803 tracks the addition of a use_openssl option. Once that bug is closed,
# use_openssl=1 can be passed; dev-libs/{nss,nspr} will no longer be needed. Note that that flag
# currently does not use system openssl but a bundled version from third_party/openssl!
# There is use_system_ssl for that

# TODO: GN build with system speech dispatcher
#  app-speech/speechd[>=0.8]

DEPENDENCIES="
    build:
        dev-lang/clang:${LLVM_SLOT}
        dev-lang/node
        dev-lang/python:*[>3]
        dev-lang/rust:*
        dev-lang/yasm
        dev-libs/compiler-rt:${LLVM_SLOT}
        dev-python/beautifulsoup4[python_abis:*(-)?]
        dev-python/html5lib[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        dev-rust/bindgen-cli
        dev-scm/git [[ note = [ https://bugs.gentoo.org/show_bug.cgi?id=593476 ] ]]
        dev-util/gperf[>=3.0.4]
        sys-devel/bison
        sys-devel/lld:${LLVM_SLOT}
        sys-devel/ninja[>=1.7.2]
    build+run:
        app-arch/brotli[>=1.1.0]
        app-arch/gzip[>=1.8]
        app-arch/zstd
        dev-libs/at-spi2-core[>=2.52.0]
        dev-libs/dbus-glib:1[>=0.84]
        dev-libs/double-conversion:=
        dev-libs/expat
        dev-libs/glib:2[>=2.26] [[ note = [ for gsettings ] ]]
        dev-libs/icu:=[>=71]
        dev-libs/libbsd
        dev-libs/libffi:=
        dev-libs/libusb:1
        dev-libs/libxml2:2.0[>=2.9.6-r1] [[ note = [ -r1 for icu support ] ]]
        dev-libs/libxslt[>=1.1.26]
        dev-libs/nspr[>=4.0]
        dev-libs/nss[>=3.26] [[ note = [ see crypto/nss_util.cc for the version requirement ] ]]
        media-libs/libavif:=
        media-libs/dav1d:=
        media-libs/flac:=
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libpng:=[>=1.2.37]
        media-libs/libwebp:=[>=0.6.0-r1]
        media-libs/openh264[>=1.6.0]
        media-libs/opus[>=1.1-r1]
        sys-apps/dbus
        sys-apps/pciutils
        sys-libs/vulkan-headers
        sys-libs/wayland
        sys-libs/zlib[>=1.2.7-r1]
        sys-sound/alsa-lib
        x11-dri/libdrm
        x11-dri/mesa
        x11-libs/cairo[X]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/harfbuzz[>=2.0.0]
        x11-libs/libevdev
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXScrnSaver
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libxkbcommon
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libxshmfence
        x11-libs/libXtst
        x11-libs/pango
        x11-proto/xcb-proto[python_abis:*(-)?]
        cups? (
            dev-libs/libgcrypt
            net-print/cups
        )
        providers:eudev? ( sys-apps/eudev )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:systemd? ( sys-apps/systemd )
        pulseaudio? ( media-sound/pulseaudio )
        vaapi? ( x11-libs/libva )
    run:
        x11-libs/gtk+:3
        cups? ( x11-libs/gtk+:3[cups] )
    recommendation:
        fonts/corefonts [[ description = [ Needed to display some websites ] ]]
        x11-apps/xdg-utils [[ description = [ Set as default browser. Use application launchers for downloaded files (only for Gnome/KDE/Xfce) ] ]]
    suggestion:
        gnome-desktop/adwaita-icon-theme [[ description = [ For the downloads tab ] ]]
"

WORK=${WORKBASE}/${MY_PNV}

chromium_pkg_setup() {
    # retpoline flag causes a build failure
    filter-flags -mindirect-branch=thunk

    export V=1

    exdirectory --allow /opt

    export CC=$(exhost --target)-clang-${LLVM_SLOT}
    export CXX=$(exhost --target)-clang++-${LLVM_SLOT}
    export AR=/usr/host/lib/llvm/${LLVM_SLOT}/bin/llvm-ar
    export NM=/usr/host/lib/llvm/${LLVM_SLOT}/bin/llvm-nm
    export READELF=$(exhost --target)-readelf

    if cxx-is-gcc ; then
        eerror "sys-devel/clang is required to build chromium-stable."
        die
    fi
}

chromium_src_prepare() {
    default

    # setup nodejs
    edo mkdir -p third_party/node/linux/node-linux-x64/bin
    edo ln -sf /usr/host/bin/node third_party/node/linux/node-linux-x64/bin/node

    # TODO: Find out how to properly do this for GN and/or report upstream
    edo sed \
        -e "s:pkg_config = \"\":pkg_config = \"$(exhost --tool-prefix)pkg-config\":g" \
        -i build/config/linux/pkg_config.gni

    # remove some of the bundled libraries
    edo build/linux/unbundle/remove_bundled_libraries.py "${CHROMIUM_KEEPLIBS[@]}" --do-remove

    # adler2 has replaced adler in rust:nightly so switch back until that becomes stable
    if ever at_least "134.0.6998.3" ; then
        edo sed -i 's/adler2/adler/' build/rust/std/BUILD.gn
    fi

    # only required to fulfill gn dependencies
    edo touch third_party/blink/tools/merge_web_test_results.pydeps
    edo mkdir -p third_party/blink/tools/blinkpy/web_tests
    edo touch third_party/blink/tools/blinkpy/web_tests/merge_results.pydeps

}

chromium_src_configure() {
    edo build/linux/unbundle/replace_gn_files.py --system-libraries ${CHROMIUM_SYSTEM_LIBS}

    local myconf_gn=""

    export AR="${AR}"
    export CC="${CC}"
    export CXX="${CXX}"
    export NM="${NM}"

    # Define a custom toolchain for GN
    myconf_gn+=" custom_toolchain=\"//build/toolchain/linux/unbundle:default\""
    myconf_gn+=" host_toolchain=\"//build/toolchain/linux/unbundle:default\""
    myconf_gn+=" clang_version=${LLVM_SLOT}"

    myconf_gn+=" rust_bindgen_root=\"/usr/$(exhost --target)\""
    myconf_gn+=" rust_sysroot_absolute=\"/usr/$(exhost --target)\""
    myconf_gn+=" rustc_version=\"$(edo /usr/$(exhost --target)/bin/rustc -V)\""

    # rust png is WIP and requires rust:nightly
    # https://crbug.com/40278281
    myconf_gn+=" enable_rust_png=false"

    # GN needs explicit config for Debug/Release as opposed to inferring it from build directory
    myconf_gn+=" is_debug=false"

    # is_official_build has nothing to do with branding, but instead creates a more optimized build
    # than normal release and disables some unsafe features useful for development
    myconf_gn+=" is_official_build=true"
    myconf_gn+=" is_cfi=false"
    myconf_gn+=" chrome_pgo_phase=0"
    # is_official_build results in max symbols which slows the build and create 5GB debug file
    # if we want to allow `=1` for better backtraces it might be worth adding a `debug` option
    myconf_gn+=" symbol_level=0 blink_symbol_level=0 v8_symbol_level=0"

    # Disable nacl, we can't build without pnacl (http://crbug.com/269560)
    myconf_gn+=" enable_nacl=false"

    myconf_gn+=" use_sysroot=false"

    myconf_gn+=" is_clang=true clang_use_chrome_plugins=false"
    myconf_gn+=" clang_base_path=\"/usr/$(exhost --build)/lib/llvm/${LLVM_SLOT}/\""

    # as of 108 v8_context_snapshot_generator linking fails with libstdc++ due to the following
    # ld.lld: error: undefined symbol: void std::__cxx11::basic_string<char16_t, std::char_traits<char16_t>, std::allocator<char16_t> >::_M_construct<char16_t const*>(char16_t const*, char16_t const*, std::forward_iterator_tag)
    myconf_gn+=" use_custom_libcxx=true"

    # Make sure that -Werror doesn't get added to CFLAGS by the build system.
    # Depending on GCC version the warnings are different and we don't want
    # the build to fail because of that.
    myconf_gn+=" treat_warnings_as_errors=false"

    # Disable fatal linker warnings, Gentoo bug 506268
    myconf_gn+=" fatal_linker_warnings=false"

    # Follow upstream advice on the chromium-packagers group
    myconf_gn+=" disable_fieldtrial_testing_config=true"

    # FFmpeg handling
    ffmpeg_branding="$(option bindist Chromium Chrome)"
    myconf_gn+=" proprietary_codecs=$(option bindist false true)"
    myconf_gn+=" ffmpeg_branding=\"${ffmpeg_branding}\""

    # Options
    myconf_gn+=" enable_hangout_services_extension=true"
    myconf_gn+=" enable_widevine=true"
    myconf_gn+=" link_pulseaudio=$(option pulseaudio true false)"
    myconf_gn+=" use_cups=$(option cups true false)"
    myconf_gn+=" use_pulseaudio=$(option pulseaudio true false)"
    myconf_gn+=" use_vaapi=$(option vaapi true false)"

    # hard disable kerberos support (requires heimdal)
    myconf_gn+=" use_kerberos=false"

    # See third_party/BUILD.gn, required for chromium to search for system harfbuzz
    myconf_gn+=" use_system_harfbuzz=true"

    # ld.lld: error: unable to find library -l:libffi_pic.a
    myconf_gn+=" use_system_libffi=true"

    if ever at_least "134.0.6998.3" ; then
        # this can be added as an OPTION if someone wants to use qt6
        myconf_gn+=" use_qt5=false use_qt6=false"
    else
        # enabled by default as of 107.0.5304.18 and adds a build-time only dependency.
        # The qt feature flag hasn't yet been toggled so GTK would be used anyway
        # https://groups.google.com/a/chromium.org/g/chromium-packagers/c/-2VGexQAK6w
        myconf_gn+=" use_qt=false"
    fi

    # conflicts with unbundling icu
    myconf_gn+=" icu_use_data_file=false"

    # Set up Google API keys, see https://www.chromium.org/developers/how-tos/api-keys
    # Note: These are for Exherbo use ONLY. For your own distribution, please get
    # your own set of keys.
    local google_api_key="AIzaSyBEuNWzExJ_qIX-NT8OY_RGN6d6ZLWnExA"
    myconf_gn+=" google_api_key=\"${google_api_key}\""

    # GN configure
    edo tools/gn/bootstrap/bootstrap.py -v --no-clean --gn-gen-args "${myconf_gn}"
    edo out/Release/gn gen --script-executable=python3 --args="${myconf_gn}" out/Release
}

chromium_src_compile() {
    if optionq suid-sandbox; then
        eninja -C out/Release mksnapshot chrome chrome_sandbox
    else
        eninja -C out/Release mksnapshot chrome
    fi

}

chromium_src_test() {
    local disabled_base_tests=(
        FilePathTest.FromUTF8Unsafe_And_AsUTF8Unsafe            # Needs a utf8 locale
        LoggingTest.CheckStreamsAreLazy                         # does not like to be sandboxed
        OutOfMemoryDeathTest.ViaSharedLibraries
        ProcessMetricsTest.GetNumberOfThreads
        RTLTest.WrapPathWithLTRFormatting
        StatsTableTest.MultipleThreads                          # Flaky
        # Need access to a running X11 server (and potentially more)
        'MessageLoopTestTypeUI.*'
        MessageLoopTest.IsType
        'WaitableEventWatcherTest.*'
        'MessagePumpGLibTest.*'
        'MessagePumpLibeventTest.*'
        'TimerTest.*'
        # fails with "out of memory" even with plenty free mem
        SecurityTest.MemoryAllocationRestrictionsNew
        SecurityTest.MemoryAllocationRestrictionsNewArray
        SecurityTest.NewOverflow
        # Fails randomly on different systems
        SysInfoTest.MaxSharedMemorySize
        TraceEventTestFixture.TraceSampling
        # 42.0.2311.135
        ProcessUtilTest.CurrentDirectory
    )
    local disabled_crypto_tests=(
        # 39.0.2171.95
        SignatureVerifierTest.VerifyRSAPSS
    )
    local disabled_gn_tests=(
        BuilderTest.BasicDeps
        # 41.0.2272.76
        Label.Resolve
    )
    local disabled_ui_base_tests=(
        # Needs fonts/corefonts
        FontTest.LoadArial

        FontTest.GetActualFontNameForTesting

        # Need access to a running X11 server (and potentially more)
        'CanvasTest.*'
        'ClipboardTest.*'
        'ClipboardTest/0.*'
        'FontTest.AvgCharWidth'
        'FontTest.AvgWidths'
        'FontTest.Widths'
        'GtkExpandedContainerTest.*'
        'RenderTextTest.*'
        'ResourceBundleTest.DelegateGetFontList'
        'ScreenTest.*'
        'TextEliderTest.*'
        'TextUtilsTest.GetStringWidth'
        'AnimationContainerTest.*'
        'AnimationTest.*'
        'SlideAnimationTest.*'
        'OSExchangeDataProviderAuraX11Test.*'
        'InputMethodBaseTest.*'
        'OSExchangeDataTest.*'

        # These have been failing for a long time but are not critical. http://crbug.com/110711
        FontListTest.FontDescString_FromFontVector
        FontListTest.Fonts_DescStringWithStyleInFlexibleFormat_RoundTrip
        FontListTest.Fonts_FontVector_RoundTrip
        FontListTest.Fonts_FromDescString
        FontListTest.Fonts_FromDescStringInFlexibleFormat
        FontListTest.Fonts_FromDescStringWithStyleInFlexibleFormat
        FontListTest.Fonts_FromFontVector
        FontListTest.Fonts_GetHeight_GetBaseline
        FontListTest.Fonts_Derive
        FontListTest.Fonts_DeriveWithSizeDelta

        # 38.0.2125.101
        SelectionRequestorTest.NestedRequests
    )

    local disabled_media_tests=(
        # http://crbug.com/160384
        'YUVConvertTest.*'
    )

    local t suites=( base cacheinvalidation crypto gn gpu printing ui_base url ) HOME="${TEMP}"

    for t in ${suites[@]}; do
        tv=disabled_${t}_tests[*]
        eninja -C out/Release ${t}_unittests
        edo out/Release/${t}_unittests --gtest_filter=$(IFS=:; echo "-${!tv}")
    done
}

chromium_src_install() {
    # Install into /opt
    local dest=/opt/${MY_PN}
    edo cd out/Release

    exeinto ${dest}
    doexe chrome
    doexe chrome_crashpad_handler

    if optionq suid-sandbox; then
        newexe chrome_sandbox chrome-sandbox # the name is hardcoded
        edo chmod 4755 "${IMAGE}"${dest}/chrome-sandbox
    fi
    doexe "${FILES}"/chromium-launcher.sh

    insinto ${dest}
    doins -r *.pak locales resources
    doins -r *.bin
    doins -r MEIPreload

    dodir /usr/$(exhost --target)/bin
    dosym ${dest}/chromium-launcher.sh /usr/$(exhost --target)/bin/${MY_PN}-browser

    # install so files
    doins *.so*
    doins vk_swiftshader_icd.json

    if [[ -d ./swiftshader ]]; then
        insinto ${dest}/swiftshader
        doins swiftshader/*.so
    fi

    for size in 24 48 64 128 256 ; do
        insinto /usr/share/icons/hicolor/${size}x${size}/apps
        newins "${WORK}"/chrome/app/theme/chromium/product_logo_${size}.png ${MY_PN}-browser.png
    done

    insinto /usr/share/applications/
    doins "${FILES}"/${MY_PN}-browser.desktop

    insinto /etc/revdep-rebuild
    hereins ${MY_PN} <<EOF
SEARCH_DIRS=${dest}
EOF
}

chromium_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

chromium_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}


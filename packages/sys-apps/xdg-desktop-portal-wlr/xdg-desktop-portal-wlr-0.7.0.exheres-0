# Copyright 2021 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=emersion release=v${PV} suffix=tar.gz ]
require meson [ cross_prefix=true ]

SUMMARY="wlroots implementation of xdg-desktop-portal"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    systemd [[ description = [ Install the systemd user service unit ] ]]

    ( providers: basu elogind systemd ) [[
        *description = [ Provider of the sd-bus library ]
        number-selected = exactly-one
    ]]
"
PLATFORMS="~amd64 ~armv8"

DEPENDENCIES="
    build:
        app-doc/scdoc[>=1.9.7]
        sys-libs/wayland-protocols[>=1.24]
        virtual/pkg-config
        systemd? (
            sys-apps/systemd [[ note = [ To get systemduserunitdir from pkg-config file ] ]]
        )
    build+run:
        dev-libs/inih
        media/pipewire[>=0.3.62]
        sys-libs/wayland[>=1.14]
        x11-dri/libdrm
        x11-dri/mesa[?X] [[ note = [ For gbm ] ]]
        providers:basu? ( sys-libs/basu )
        providers:elogind? ( sys-auth/elogind )
        providers:systemd? ( sys-apps/systemd )
    run:
        sys-apps/xdg-desktop-portal[>=1.5]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dman-pages=enabled
)
MESON_SRC_CONFIGURE_OPTIONS=(
    'providers:basu -Dsd-bus-provider=basu'
    'providers:elogind -Dsd-bus-provider=libelogind'
    'providers:systemd -Dsd-bus-provider=libsystemd'
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    systemd
)


# Copyright 2011 Paul Seidler
# Copyright 2018 Bjorn Pagen <bjornpagen@gmail.com>
# Copyright 2024 Ali Polatel <alip@hexsys.org>
# Distributed under the terms of the GNU General Public License v2

require meson
require systemd-service
require python [ blacklist=2 multibuild=false ]
require vala [ vala_dep=true with_opt=true option_name=vapi ]
require option-renames [ renames=[ 'systemd providers:systemd' ] ]
require test-dbus-daemon

SUMMARY="D-Bus interfaces for querying and manipulating user account information"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/AccountsService"
DOWNLOADS="https://www.freedesktop.org/software/${PN}/${PNV}.tar.xz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    doc
    gobject-introspection
    gtk-doc
    vapi [[ requires = gobject-introspection ]]

    ( libc: musl )
    ( providers: elogind systemd ) [[
        *description = [ Login daemon provider ]
        number-selected = exactly-one
    ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
        doc? (
            app-text/docbook-xml-dtd:4.1.2
            app-text/xmlto
            dev-libs/libxslt
        )
        gtk-doc? ( dev-doc/gtk-doc[>=1.15] )
    build+run:
        dev-libs/glib:2[>=2.63.5][gobject-introspection(+)?]
        sys-apps/dbus[>=1.9.18]
        sys-auth/polkit:1
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.12] )
        !libc:musl? ( dev-libs/libxcrypt:= )
        providers:systemd? ( sys-apps/systemd[>=186] )
        providers:elogind? ( sys-auth/elogind[>=229.4] )
    test:
         gobject-introspection? (
             dev-python/python-dbusmock[python_abis:*(-)?]
             gnome-bindings/pygobject[python_abis:*(-)?]
        )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Add-paludisbuild-to-excluded-users.patch
    "${FILES}"/0002-Musl-libc-does-not-support-fgetspent_r-so-fall-back-.patch
    "${FILES}"/musl-wtmp.patch
    "${FILES}"/da65bee12d9118fe1a49c8718d428fe61d232339.patch
    "${FILES}"/${PNV}-tests-s-assertEquals-assertEqual.patch
    "${FILES}"/${PN}-Skip-two-tests-which-want-to-access-user-dirs.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    -Dadmin_group=wheel
    -Dminimum_uid=1000
    -Dsystemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc docbook'
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    'providers:elogind elogind'
    vapi
)

src_test() {
    test-dbus-daemon_run-tests meson_src_test
}

src_install() {
    meson_src_install

    keepdir /var/lib/AccountsService/{users,icons}
}


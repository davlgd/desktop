# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix=https://gitlab.freedesktop.org new_download_scheme=true tag=v${PV} suffix=tar.bz2 ] \
    66-service \
    systemd-service \
    udev-rules \
    zsh-completion \
    option-renames [ renames=[ 'systemd providers:systemd' ] ] \
    meson

SUMMARY="An abstraction for enumerating power devices"
HOMEPAGE+=" https://${PN}.freedesktop.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    idevice [[ description = [ Enable support for iPod, iPad, and iPhone battery status ] ]]
    ( providers: elogind pm-utils systemd ) [[
        *description = [ Suspend/Resume provider ]
        number-selected = at-most-one
    ]]
"

# uses system dbus socket
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.21]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.9] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.11] )
    build+run:
        dev-libs/glib:2[>=2.66.0][gobject-introspection(+)?]
        gnome-desktop/libgudev[>=238]
        sys-auth/polkit:1[>=0.103]
        idevice? (
            app-pda/libimobiledevice:1.0[>=0.9.7]
            dev-libs/libplist:2.0
        )
    run:
        providers:elogind? ( sys-auth/elogind )
        providers:pm-utils? ( sys-power/pm-utils[>=1.4.1] )
        providers:systemd? ( sys-apps/systemd )
"

MESON_SOURCE="${WORKBASE}/${PN}-v${PV}"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dhistorydir=/var/lib/upower
    -Dman=true
    -Dos_backend=linux
    -Dpolkit=enabled
    -Dstatedir=/var/lib/upower
    -Dsystemdsystemunitdir="${SYSTEMDSYSTEMUNITDIR}"
    -Dudevhwdbdir="${UDEVHWDBDIR}"
    -Dudevrulesdir="${UDEVRULESDIR}"
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    gtk-doc
    "zsh-completion zshcompletiondir ${ZSHCOMPLETIONDIR} no"
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gobject-introspection introspection'
    idevice
)

src_install() {
    meson_src_install

    install_66_files

    # history directory is created at runtime if missing
    edo rm -r "${IMAGE}"/var
}

pkg_postinst() {
    local cruft=( /etc/dbus-1/system.d/org.freedesktop.UPower.conf )
    for file in ${cruft[@]}; do
        if test -f "${file}" ; then
            nonfatal edo rm "${file}" || ewarn "removing ${file} failed"
        fi
    done
}


# Copyright 2016 Julian Ospald <hasufell@posteo.de>
# Copyright 2024 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user='projectatomic' suffix="tar.xz" release="v${PV}" ]
require bash-completion zsh-completion
require meson

SUMMARY="Unprivileged sandboxing tool"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    selinux
"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-stylesheets
        dev-lang/python:*[>=3]
        dev-libs/libxslt
        sys-kernel/linux-headers[>=4.3]
        virtual/pkg-config
    build+run:
        sys-libs/libcap
        selinux? ( security/libselinux[>=2.1.9] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dbash_completion_dir=${BASHCOMPLETIONDIR}
    -Dzsh_completion_dir=${ZSHCOMPLETIONDIR}

    -Dman=enabled
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'bash-completion bash_completion'
    'selinux'
    'zsh-completion zsh_completion'
)
MESON_SRC_CONFIGURE_TESTS=( '-Dtests=true -Dtests=false' )

src_prepare() {
    meson_src_prepare

    # syd-3: /var/tmp is ro /tmp is private.
    edo sed -i \
            -e '/^tempdir/s|/var/tmp|/tmp|' \
            tests/libtest.sh
}


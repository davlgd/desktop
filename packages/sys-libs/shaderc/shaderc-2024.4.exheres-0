# Copyright 2018-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=google tag=v${PV} ] \
    cmake

SUMMARY="Collection of tools, libraries, and tests for Vulkan shader compilation"
DESCRIPTION="
A collection of tools, libraries and tests for shader compilation. At the moment it includes:
* glslc, a command line compiler for GLSL/HLSL to SPIR-V, and
* libshaderc, a library API for doing the same.
Shaderc wraps around core functionality in glslang and SPIRV-Tools. Shaderc aims to to provide:
* a command line compiler with GCC- and Clang-like usage, for better integration with build systems
* an API where functionality can be added without breaking existing clients
* an API supporting standard concurrency patterns across multiple operating systems
* increased functionality such as file #include support
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

# Fails its tests, last checked: 2023.8
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        sys-libs/spirv-headers[>=1.5.5-r23]
    build+run:
        dev-lang/glslang[>=15.1.0]
        dev-lang/spirv-tools[>=2024.4-rc2]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-2020.5-drop-third-party-code.patch
    "${FILES}"/${PN}-2020.1-fix-spirv-includes-location.patch
    "${FILES}"/${PN}-2023.8-glslang-linker-flags.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DENABLE_CODE_COVERAGE:BOOL=FALSE
    -DSHADERC_ENABLE_WERROR_COMPILE:BOOL=FALSE
    -DSHADERC_ENABLE_WGSL_OUTPUT:BOOL=FALSE
    -DSHADERC_SKIP_COPYRIGHT_CHECK:BOOL=FALSE
    -DSHADERC_SKIP_EXAMPLES:BOOL=TRUE
    -DSHADERC_SKIP_INSTALL:BOOL=FALSE
    -DSHADERC_SKIP_TESTS:BOOL=TRUE
)

src_prepare() {
    cmake_src_prepare

    # remove build-version attempt
    edo sed \
        -e '/build-version/d' \
        -i glslc/CMakeLists.txt
    # required when not building from git
    local spirv_tools_ver=$(best_version dev-lang/spirv-tools)
    local glslang_ver=$(best_version dev-lang/glslang)
    edo cat > glslc/src/build-version.inc << EOF
"${PNV}\n"
"${spirv_tools_ver%%:*}\n"
"${glslang_ver%%:*}\n"
EOF
}


# Copyright 2018-2020 Danilo Spinella <danyspin97@protonmail.com>
# Copyright 2019 Maxime SORIN <maxime.sorin@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

require cargo [ disable_default_features=true rust_minimum_version=1.74.0 ]
# Keep after cargo, so we don't fetch the crates.io tarball
require github [ user='alacritty' tag=v${PV} ]
require bash-completion zsh-completion

if ever is_scm; then
    # cargo.exlib sets DOWNLOADS incorrectly for scm version
    DOWNLOADS=""
fi

# Provided man pages from archive are raw .scd
# Upstream supplies compiled version shipped separately
DOWNLOADS+=" https://github.com/${PN}/${PN}/releases/download/v${PV}/${PN}.1.gz "
DOWNLOADS+=" https://github.com/${PN}/${PN}/releases/download/v${PV}/${PN}-msg.1.gz "
DOWNLOADS+=" https://github.com/${PN}/${PN}/releases/download/v${PV}/${PN}.5.gz "
DOWNLOADS+=" https://github.com/${PN}/${PN}/releases/download/v${PV}/${PN}-bindings.5.gz "

export_exlib_phases src_install

SUMMARY="A cross-platform, GPU-accelerated terminal emulator"
DESCRIPTION="
Alacritty is focused on simplicity and performance. The performance goal means it should be faster
than any other terminal emulator available. The simplicity goal means that it doesn't have features
such as tabs or splits (which can be better provided by a window manager or terminal multiplexer)
nor niceties like a GUI config editor.
"

LICENCES="Apache-2.0"
SLOT="0"

MYOPTIONS="
    fish-completion [[ description = [ Install completion files for the fish shell ] ]]
    ( X wayland ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build+run:
        media-libs/fontconfig
        media-libs/freetype:=
        sys-libs/zlib
        x11-dri/mesa[?X]
    run:
        sys-libs/ncurses[>=6.2] [[
            note = [ First version to ship 'alacritty' terminfo ]
        ]]
"

ECARGO_FEATURE_ENABLES=(
    'X x11'
    'wayland wayland'
)

ZSH_COMPLETIONS=( extra/completions/_${PN} )
BASH_COMPLETIONS=( extra/completions/${PN}.bash )

alacritty_src_install() {
    edo cd "${ALACRITTY_ROOT}"
    dobin "target/$(rust_target_arch_name)/release/${PN}"

    insinto /usr/share/applications
    doins extra/linux/Alacritty.desktop

    insinto /usr/share/pixmaps
    newins extra/logo/${PN}-term.svg Alacritty.svg

    doman "${WORKBASE}"/${PN}.1
    doman "${WORKBASE}"/${PN}-msg.1
    doman "${WORKBASE}"/${PN}.5
    doman "${WORKBASE}"/${PN}-bindings.5

    emagicdocs

    zsh-completion_src_install
    bash-completion_src_install
    if option fish-completion; then
        insinto /usr/share/fish
        newins extra/completions/${PN}.fish ${PN}.fish
    fi
}

